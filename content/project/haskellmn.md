+++
title = "HaskellMN"
description = "Minneapolis Haskell Meetup Group"
date = "2018-09-23T15:22:00Z"
author = "Trevor Riles"
+++

## About Project

HaskellMN is a monthly meetup group located in Minneapolis, MN. I am currently the organizer of the group and present occasionaly.

https://meetup.com/haskellmn
