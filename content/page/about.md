+++
title = "About"
date = 2018-09-23T15:17:00Z
author = "Trevor Riles"
description = "Things about me."
+++

## About

Hello, I'm Trevor and I am a Site Reliability Engineer at SPS Commerce. When I'm not improving our deployment and monitoring strategies at work I enjoy running and other fitness related activities.
